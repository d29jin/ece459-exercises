// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut total = 0;
    for iterable in 0..number
    {
        if iterable%multiple1 == 0 || iterable%multiple2 == 0
        {
            total += iterable;
        }
    }
    return total;
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
